//
//  AccountModel.m
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import "AccountModel.h"

@implementation AccountModel


- (id)init
{
    if (self = [super init])
    {
        
    }
    
    return self;
}

- (NSDictionary*)turnToDic
{
    NSMutableDictionary *targetDic = [NSMutableDictionary dictionaryWithCapacity:3];
    
    [targetDic setValue:[NSString stringWithFormat:@"%ld",self.m_accountType] forKey:@"accountType"];
    [targetDic setValue:[NSString stringWithFormat:@"%g",self.m_targetMoney] forKey:@"targetMoney"];
    [targetDic setValue:[NSString stringWithFormat:@"%ld",self.m_moneySource] forKey:@"moneySource"];
    
    
    return targetDic;
}


@end
