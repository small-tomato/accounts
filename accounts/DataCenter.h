//
//  DataCenter.h
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountModel.h"

@interface DataCenter : NSObject

@property (retain, nonatomic) NSMutableArray *m_familyMumberArray;
@property (retain, nonatomic) NSMutableArray *m_saveStyleArray;
@property (retain, nonatomic) NSMutableArray *m_accountDataArray;




- (id)init;

+ (DataCenter*)shareDataCenter;

- (void)loadUserData;

- (BOOL)addFamilyMumber:(NSString*)familyMumber;

- (BOOL)removeFamilyMumber:(NSInteger)mumberIndex;

- (BOOL)addSaveStyle:(NSString*)saveStyle;

- (BOOL)removeSaveStyle:(NSInteger)styleIndex;

- (BOOL)saveOneAccount:(AccountModel*)aAccountModel;

- (BOOL)fixedOneAccount:(AccountModel*)aAccountModel accountIndex:(NSInteger)accountIndex;

- (BOOL)removeOneAccount:(NSInteger)accountIndex;


@end
