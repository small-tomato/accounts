//
//  DropDownView.h
//  account
//
//  Created by Frank on 14-10-9.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DropDownViewDelegate <NSObject>


@required

- (CGFloat)heightOfCell;

- (NSInteger)maxDisplayedCell;

@optional

- (void)didSelectedIndex:(NSInteger)selectedIndex targetString:(NSString*)targetStr;

@end

@interface DropDownView : UIView <UITableViewDataSource,UITableViewDelegate>


@property (readonly, nonatomic) id<DropDownViewDelegate> m_delegate;
@property (readonly, retain, nonatomic) NSMutableArray *m_dataArray;
@property (readonly, nonatomic) BOOL isDown;
@property (readwrite, nonatomic) NSUInteger nameNumber;

- (id)initWithFrame:(CGRect)frame dataArray:(NSArray*)dataArray delegate:(id<DropDownViewDelegate>) delegate;

- (void)tapDown;

- (void)tapUp;

- (void)addDataString:(NSString*)dataStr;


@end
