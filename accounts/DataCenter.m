//
//  DataCenter.m
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import "DataCenter.h"



@implementation DataCenter

{
    NSFileManager *fileM;
    NSString *plistFilePath;
}

- (id)init
{
    if (self = [super init])
    {
        fileM = [NSFileManager defaultManager];
        plistFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, NO) lastObject] pathForAuxiliaryExecutable:@"UserAccount.plist"];
    }
    
    
    return self;
}

+ (DataCenter*)shareDataCenter
{
    static DataCenter *m_shareDataCenter = nil;
    if (m_shareDataCenter == nil)
    {
        m_shareDataCenter = [[DataCenter alloc]init];
    }
    
    
    return m_shareDataCenter;
}



- (void)loadUserData
{
    if ([fileM fileExistsAtPath:plistFilePath] == NO)
    {
        NSError *error;
        
        while ([fileM copyItemAtPath:[[NSBundle mainBundle]pathForResource:@"UserAccount" ofType:@"plist"] toPath:plistFilePath error:&error] == NO)
        {
            NSLog(@"error : %@",error);
        }
    }
    
    NSDictionary *userAccountDic = [NSDictionary dictionaryWithContentsOfFile:plistFilePath];
    
    self.m_familyMumberArray = [NSMutableArray arrayWithArray:[userAccountDic valueForKey:@"FamilyMumber"]];
    
    self.m_saveStyleArray = [NSMutableArray arrayWithArray:[userAccountDic valueForKey:@"SaveStyle"]];
    
    self.m_accountDataArray = [NSMutableArray arrayWithArray:[userAccountDic valueForKey:@"AccountData"]];
}

- (BOOL)addFamilyMumber:(NSString*)familyMumber
{
    return YES;
}

- (BOOL)removeFamilyMumber:(NSInteger)mumberIndex
{
    return YES;
}

- (BOOL)addSaveStyle:(NSString*)saveStyle
{
    return YES;
}

- (BOOL)removeSaveStyle:(NSInteger)styleIndex
{
    return YES;
}

- (BOOL)saveOneAccount:(AccountModel*)aAccountModel
{
    return YES;
}

- (BOOL)fixedOneAccount:(AccountModel*)aAccountModel accountIndex:(NSInteger)accountIndex
{
    return YES;
}

- (BOOL)removeOneAccount:(NSInteger)accountIndex
{
    return YES;
}


@end
