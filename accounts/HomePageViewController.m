//
//  HomePageViewController.m
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import "HomePageViewController.h"

@interface HomePageViewController ()

@end

@implementation HomePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    self.dataArray = [NSMutableArray arrayWithArray:@[@"Me",@"Frank",@"Alex",@"Nike",@"Waster",@"AAA",@"BBB"]];
    
    CGRect aRect = CGRectMake(50, 50, 100, 20);
    
    self.dropDV = [[DropDownView alloc]initWithFrame:aRect dataArray:self.dataArray delegate:self];
    
    [self.view addSubview:self.dropDV];
    
//    self.testButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [self.testButton setTitle:@"Click" forState:UIControlStateNormal];
//    
//    self.testButton.frame = CGRectMake(0, 0, 100, 20);
//    
//    [self.testButton addTarget:self action:@selector(onButton:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.dropDV addSubview:self.testButton];
    
}

- (CGFloat)heightOfCell
{
    return 30;
}

- (NSInteger)maxDisplayedCell
{
    return 5;
}

- (void)didSelectedIndex:(NSInteger)selectedIndex targetString:(NSString *)targetStr
{
    NSLog(@"seletedIndex = %ld,targetStr = %@,dataStr = %@",selectedIndex,targetStr,[self.dataArray objectAtIndex:selectedIndex]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
