//
//  HomePageViewController.h
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface HomePageViewController : UIViewController <DropDownViewDelegate>


@property (retain, nonatomic) NSMutableArray *dataArray;
@property (retain, nonatomic) DropDownView *dropDV;

@end
