//
//  AccountModel.h
//  account
//
//  Created by Frank on 14-9-29.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, AccountType)
{
    AccountTypeNone = 0,          // 不记入
    AccountTypeDisbursement = 1,  // 支出
    AccountTypeIncome = 2,        // 收入
};

typedef NS_ENUM(NSInteger, MoneyType)
{
    MoneyTypeNone = 0,
    MoneyTypeCash = 1,
    MoneyTypeCard = 2,
};


@interface AccountModel : NSObject

//支出账目分为衣食住行四种；
//收入账目为工资等；

//收入和支出账目的目标人均来自familyMumber


@property (readwrite, nonatomic) AccountType m_accountType;
@property (readwrite, nonatomic) CGFloat m_targetMoney;
@property (readwrite, nonatomic) NSInteger m_moneySource;



- (id)init;

- (NSDictionary*)turnToDic;








@end
