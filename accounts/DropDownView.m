//
//  DropDownView.m
//  account
//
//  Created by Frank on 14-10-9.
//  Copyright (c) 2014年 buggroup. All rights reserved.
//

#import "DropDownView.h"

@implementation DropDownView

{
    UIButton *displayedButton;
    UITableView *dataTableView;
    CGRect startFrame;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame dataArray:(NSArray*)dataArray delegate:(id<DropDownViewDelegate>) delegate
{
    if (self = [super initWithFrame:frame])
    {
        _m_dataArray = [NSMutableArray arrayWithArray:dataArray];
        _m_delegate = delegate;
        startFrame = frame;
        _isDown = NO;
        _nameNumber = -1;
        
        
        displayedButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [displayedButton setTitle:@"Click" forState:UIControlStateNormal];
        
        displayedButton.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        
        [displayedButton addTarget:self action:@selector(onButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [displayedButton setTintColor:[UIColor blackColor]];
        
        [self addSubview:displayedButton];
    }
    
    return self;
}

- (void)onButton:(UIButton*)sender
{
    if (_isDown)
    {
        [self tapUp];
    }
    else
    {
        [self tapDown];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _m_dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [_m_delegate heightOfCell];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellStr = @"cellString";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellStr];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    cell.textLabel.text = [_m_dataArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tapDown
{
    _isDown = YES;
    
    if (!dataTableView)
    {
        dataTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 0) style:UITableViewStylePlain];
        dataTableView.delegate = self;
        dataTableView.dataSource = self;
        
        [self addSubview:dataTableView];
    }
    
    CGFloat tableH = 0;
    NSInteger maxCellNum = [_m_delegate maxDisplayedCell];
    CGFloat heightOfCell = [_m_delegate heightOfCell];
    
    if (_m_dataArray.count < maxCellNum)
    {
        tableH = heightOfCell*_m_dataArray.count;
    }
    else
    {
        tableH = heightOfCell*maxCellNum;
    }
    
    
    
    [UIView beginAnimations:nil context:nil];
    
    self.frame = CGRectMake(startFrame.origin.x, startFrame.origin.y, startFrame.size.width, startFrame.size.height+tableH);
    dataTableView.frame = CGRectMake(dataTableView.frame.origin.x, dataTableView.frame.origin.y, dataTableView.frame.size.width, tableH);
    
    
    [UIView commitAnimations];
    
    
}

- (void)tapUp
{
    _isDown = NO;
    
    [UIView beginAnimations:nil context:nil];
    
    self.frame = startFrame;
    dataTableView.frame = CGRectMake(dataTableView.frame.origin.x, dataTableView.frame.origin.y, dataTableView.frame.size.width, 0);
    
    [UIView commitAnimations];
}

- (void)addDataString:(NSString*)dataStr
{
    [_m_dataArray addObject:dataStr];
    
    [dataTableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [displayedButton setTitle:[_m_dataArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    
    [self tapUp];
    
    if ([self.m_delegate respondsToSelector:@selector(didSelectedIndex: targetString:)])
    {
        [self.m_delegate didSelectedIndex:indexPath.row targetString:[_m_dataArray objectAtIndex:indexPath.row]];
    }
}




@end
